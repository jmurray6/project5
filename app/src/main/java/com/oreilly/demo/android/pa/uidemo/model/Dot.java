package com.oreilly.demo.android.pa.uidemo.model;
import java.awt.*;
import java.awt.event.*;


/** A dot: the coordinates, color and size. */
public final class Dot {
    private final float x, y;
    private final int color;
    private final int diameter;
    //private state s;
    //private square sq;
    //update();
    //check;
    //private int lives;

    /**
     * @param x horizontal coordinate.
     * @param y vertical coordinate.
     * @param color the color.
     * @param diameter dot diameter.
     */
    public Dot(final float x, final float y, final int color, final int diameter) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.diameter = diameter;
    }

    /** @return the horizontal coordinate. */
    public float getX() { return x; }

    /** @return the vertical coordinate. */
    public float getY() { return y; }//change coordinates to grid squares

    /** @return the color. */
    public int getColor() { return color; }

    /** @return the dot diameter. */
    public int getDiameter() { return diameter; }

    public State state;

    public int lives;

    public void addlistener (actionlistener a);


}

//we will turn this into monster
//change color when state changes
//display number of lives on dot
//check function
//update function
//state class
//add update method on tick

/**
 * make two listeners: countdown listener and touch listener
 * run update on each listener
 * update uses check
 **/