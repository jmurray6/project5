package com.oreilly.demo.android.pa.uidemo.model;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
 * Created by jonahmurray on 12/11/16.
 */

public interface TouchListener {
    void ActionPerformed (ActionEvent action);
}
